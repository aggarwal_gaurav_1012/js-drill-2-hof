function convertSalaryToNumber(inventory) {
    return inventory.map(item => {
        return {

            // to create a shallow copy of the 'item' object
            ...item,

            // to convert the salary value from a string to a number
            salary: parseFloat(item.salary.replace('$', ''))
        };
    });
}

module.exports = convertSalaryToNumber;