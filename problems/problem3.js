function correctSalaries(inventory) {
    return inventory.map(item => ({

        // to create a shallow copy of the 'item' object
        ...item,

        // for corrected_salary
        corrected_salary: parseFloat(item.salary.replace('$', '')) * 10000
    }));
}

module.exports = correctSalaries;