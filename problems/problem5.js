// function to find the sum of all salaries based on country. ( Group it based on country and then find the sum ).
function sumSalariesByCountry(inventory) {
    const salariesByCountry = inventory.reduce((acc, employee) => {
        const country = employee.location;
        const salary = parseFloat(employee.salary.replace('$', ''));
        acc[country] = (acc[country] || 0) + salary;
        return acc;
    }, {});

    return salariesByCountry;
}

module.exports = sumSalariesByCountry;