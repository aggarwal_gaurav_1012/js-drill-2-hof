function findWebDevelopers(inventory) {

    // to find all Web Developers. ( It could be Web Developer III or Web Developer II or anything else )
    return inventory.filter(employee => employee.job.includes("Web Developer"));
}

module.exports = findWebDevelopers;