function calculateAverageSalaryByCountry(inventory) {

    // Group the data based on country
    const groupedByCountry = inventory.reduce((acc, employee) => {
        const country = employee.location;
        if (!acc[country]) {
            acc[country] = [];
        }
        acc[country].push(employee.salary);
        return acc;
    }, {});

    // Calculate total salary and count employees for each country
    const averagesByCountry = Object.keys(groupedByCountry).map(country => {
        const totalSalary = groupedByCountry[country].reduce((sum, salary) => sum + parseFloat(salary.slice(1)), 0);
        const employeeCount = groupedByCountry[country].length;
        const averageSalary = totalSalary / employeeCount;
        return { country, averageSalary };
    });

    return averagesByCountry;
}

module.exports = calculateAverageSalaryByCountry;