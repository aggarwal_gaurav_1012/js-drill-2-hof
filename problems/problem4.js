// Function to find the sum of all salaries
function sumSalaries(inventory) {
    
    // Extract salaries using map
    const salaries = inventory.map(item => parseFloat(item.salary.replace('$', '')));
    
    // Calculate the sum using reduce
    const totalSalary = salaries.reduce((acc, curr) => acc + curr, 0);
    
    return totalSalary;
}

module.exports = sumSalaries;