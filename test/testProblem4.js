const sumSalaries = require('../problems/problem4');

const inventory = require('../js_drill_2.cjs');

// Test the sumSalaries function
const totalSalary = sumSalaries(inventory);
console.log('Total Salary:', totalSalary);